#! /bin/bash
## This script is used to run the project. It shuold contain the script which will run the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user
set -e

# Run the application
/usr/bin/java -jar target/comp_job_java_2-0.0.1.jar
